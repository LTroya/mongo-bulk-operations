const fs = require('fs');
const json = require('big-json');

const MongoClient = require('mongodb').MongoClient;

const readStream = fs.createReadStream('price-list.json');
const parseStream = json.createParseStream();

const date = new Date().getTime();
const url = 'mongodb://localhost:27017';
const dbName = 'linets';

parseStream.on('data', function(list) {
    const products = list.Table;

    MongoClient.connect(url, (err, client) => {
        if (err) {
            console.log(err);
            return;
        }

        const db = client.db(dbName);
        const collection = db.collection('products');
        const batch = collection.initializeUnorderedBulkOp();

        products.forEach(product => {
            batch.insert({ ...product, date });
        });

        batch.execute((err, result) => {
            console.log('Products saved!');
            client.close();
        });
    });
});

readStream.pipe(parseStream);
